from django.shortcuts import render
from django.http import HttpResponse
from .models import studentdata,coursedata


# Create your views here.

# def hello_world(request):
# 	return HttpResponse("Hello!! world \n")


# def hello_world(request):
# 	f = open('/home/tuxfux/Documents/LFL-Django-101/src/blog/templates/test.html')
# 	content = f.read()
# 	return HttpResponse(content)

### time to render our home page.
def home_page(request):
	context = {}
	return render(request,'blog/home.html',context)

### test.html will be looked into the apps folder.
def hello_world(request):
	context = {}
	return render(request,'test2.html',context)

## creation of my first blog page.
def Studentdata(request):
	## for my static page
	#context = {}
	#return render(request,'blog/myblog.html',context)
	## passing variable data to the template
	# context = {'name1':'mounika','name2':'krithika','name3':'santosh',
	# 		  'course1':'django','course2':'statics','course3':'data analysis',
	# 		  'loc1':'india','loc2':'india','loc3':'india'}
	# context = {'namesdb':[{'name':'mounika','course':'django','loc':'india'},
	# 					  {'name':'krithika','course':'statics','loc':'india'},
	# 					  {'name':'santosh','course':'data analysis','loc':'india'}]}
	student_data = studentdata.objects.all()  # querying happending from backend model
	context = {'studentdb': student_data }
	return render(request,'blog/studentdata.html',context)

def Coursedata(request):
	course_data  = coursedata.objects.all()
	context = {'coursedb': course_data }
	return render(request,'blog/coursedata.html',context)


# https://docs.djangoproject.com/en/3.0/topics/forms/#the-view

from .forms import StudentDataForm,CourseDataForm

def studentview(request):
	# POST
	# if this is a POST request we need to process the form data
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = StudentDataForm(request.POST)
		# check whether it's valid:
		if form.is_valid():
			# process the data in form.cleaned_data as required
			# https://docs.djangoproject.com/en/3.0/ref/forms/api/#django.forms.Form.cleaned_data
			student_name = form.cleaned_data['student_name']
			student_location = form.cleaned_data['student_location']
			student_email = form.cleaned_data['student_email']
			print (student_name,student_location,student_email)
			studentdata.objects.create(name=student_name,location=student_location,email=student_email)
			return HttpResponse("Thank you for entering the data \n")

			# redirect to a new URL:
			#return HttpResponseRedirect('/thanks/')
	else:		
	# GET or in case data NOT VALID
		studentform = StudentDataForm # created a class not a instance
		context = {'form':studentform}
		return render(request,'blog/StudentForm.html',context)

def courseview(request):
	# POST
	# if this is a POST request and populate it with data from request
	if request.method == 'POST':
		form = CourseDataForm(request.POST)
		if form.is_valid():
			course_id = form.cleaned_data['course_id']
			course = form.cleaned_data['course']
			student_id = form.cleaned_data['student_id']
			day_of_week = form.cleaned_data['day_of_week']
			print (course_id,course,student_id,day_of_week)
			coursedata.objects.create(course_id=course_id,course=course,student_id=student_id,day_of_week=day_of_week)
			return HttpResponse("Thank you for entering the data \n") 

	else:
		# GET or in case data NOT VALID
		courseform = CourseDataForm
		context = {'form':courseform}
		return render(request,'blog/CourseForm.html',context)
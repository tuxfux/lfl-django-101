from django.db import models
from django.utils import timezone

# Create your models here.
# https://docs.djangoproject.com/en/3.0/topics/db/models/
# https://docs.djangoproject.com/en/3.0/ref/models/fields/#model-field-types

# create your models here.

class Post(models.Model):
	author = models.ForeignKey('auth.User',on_delete=models.CASCADE)
	title = models.CharField(max_length=200)
	text = models.TextField()
	created_date = models.DateTimeField(default=timezone.now)
	published_date = models.DateTimeField(blank=True,null=True) # null -> database,blank -> forms

	def publish(self):
		self.published_date = timezone.now()
		self.save()

	def __str__(self):
		return self.title

## A very basic model

class studentdata(models.Model):
	name = models.CharField(max_length=50)
	location = models.CharField(max_length=20)
	email = models.EmailField(max_length=30,null=True,blank=True) # null -> database , blank -> forms

	def __str__(self):
		return self.name

## TODO - is it possible to make a new primary key by ignoring already created primary key.
class coursedata(models.Model):
	week = [
    ('sun', 'sunday'),
    ('mon', 'monday'),
    ('tue', 'tuesday'),
    ('wed', 'wednesday'),
    ('thu', 'thursday'),
    ('fri','friday'),
    ('sat','saturday'),
	]
	course_id = models.CharField(max_length=5,unique=True,blank=True)
	course = models.CharField(max_length=20)
	student_id = models.ForeignKey(studentdata,on_delete=models.CASCADE)
	day_of_week = models.CharField(max_length=3,choices=week) 

	def __str__(self):
		return ("{} is studying {}".format(self.student_id,self.course))


from django.urls import path
from .views import studentview,courseview,Studentdata,Coursedata

urlpatterns = [
    path('studentForm/',studentview,name="studentForm"),
    path('courseForm/',courseview,name="courseForm"),
    path('Studentdata/',Studentdata,name="studentdata"),
    path('Coursedata/',Coursedata,name="coursedata"),
]

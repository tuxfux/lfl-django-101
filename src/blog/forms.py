from django import forms


## static form
class StudentDataForm(forms.Form):
	student_name = forms.CharField(max_length=50,required=True,label="Student Name")
	student_location = forms.CharField(max_length=20,required=True,label="Student Location")
	student_email = forms.EmailField(required=False,label="Student Email")


## form fields created dur to model
from .models import coursedata

## modular form
class CourseDataForm(forms.ModelForm):
	class Meta:
		model = coursedata
		fields = '__all__'
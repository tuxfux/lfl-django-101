from django.contrib import admin
from .models import studentdata
from .models import coursedata

# Register your models here.
## https://docs.djangoproject.com/en/3.0/ref/contrib/admin/#modeladmin-objects

admin.site.register(studentdata)
admin.site.register(coursedata)
